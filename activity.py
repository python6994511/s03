# The goal of the activity is to be able to create a class with properties and methods, and to create an object instance of the class.

# 1 Create a Class called Camper and give it the attributes name, batch, course_type
# 2 Create a method called career_track which will print out the string Currently enrolled in the <value of course_type> program.
# 3 Create a method called info which will print out the string My name is <value of name> of batch <value of batch>.
# 4 Create an object from class Camper called zuitt_camper and pass in arguments for name, batch, and course_type.
# 5 Print the value of the object's name, batch, course type.
# 6 Execute the info method and career_track of the object.

# 1
class Camper():
    def __init__(self, name, batch, course_type):
        self.name = name;
        self.batch = batch;
        self.course_type = course_type;
    # 2
    def career_track(self):
        print(f"Current enrolled value: {self.course_type}.");
    # 3
    def info(self):
        print(f"My name is {self.name} of batch {self.batch}.");
# 4
zuitt_camper = Camper("Josh", 100, "Full-StackJS(?)");
# 5
print(zuitt_camper.__dict__);
# 6
zuitt_camper.info();
zuitt_camper.career_track();
